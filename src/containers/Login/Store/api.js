import axios from 'axios';
import { API_URL } from '../../../utils/constants';


export const userLoginApi = async (postData) => {
  try {
   const response = await axios({
    method: 'POST',
    url: `${API_URL}login`,
    data: {'username': postData.email, 'password': postData.password},
    headers: {'Content-Type': 'application/json', 'Authorization' : null},
  });
  if(response.code === 200) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${response && response.data && response.data.token}`;
    return response && response.data 
  } else {
    return response
  }
   
  } catch (e) {
    return e;
  }
};

export const logoutApi = async () => { 
  try {
    const response = await axios({
     method: 'GET',
     url: `${API_URL}logout`,
     headers: {'Content-Type': 'application/json'},
   });
   axios.defaults.headers.common['Authorization'] = null;
     return response
   } catch (e) {
     return e;
   }
}
