import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { PropTypes } from 'prop-types';
import InputField from '../../component/inputField';
import ButtonField from '../../component/button';
import loader from '../../images/loader.gif';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  static propTypes = {
    getUserLogin: PropTypes.func,
    LoginStore: PropTypes.any,
  };


  static getDerivedStateFromProps(props) {
    const { LoginStore: { user }, history } = props;
    if (user && user.token !== undefined) {
      history.push('/dashboard');
    }
    return null;
  }

  onChange = (e) => {
    // if( e.target.name === 'email') {
    //   this.setState({ [e.target.name]: (e.target.value).toLowerCase()});
    // } else {
    //   this.setState({ [e.target.name]: e.target.value });
    // }
    this.setState({ [e.target.name]: e.target.value });
  }

  /**
   * Login Function
   */
  loginUser = () => {
    const { getUserLogin } = this.props;
    let { email, password } = this.state;
    email = email.toLowerCase();
    getUserLogin({ email, password });
  }


  render() {
    const { email, password } = this.state;
    const { LoginStore:{phase, user} } = this.props;
    return (
      <>
      <div className="sufee-login d-flex align-content-center flex-wrap">
       
      <div className="container">
      <div className="login-content">
          <div className="login-logo">
              <Link to="/">
                  <img className="align-content" src="https://www.hylamobile.com/wp-content/themes/hyla-mobile/images/hyla-logo-green.svg" alt="Roooter UI"/>
              </Link>
          </div>

          {phase === "LOADING" ? (
          <div className='siteloader'>
            <div className='siteloader-inner'>
              <img src={loader} alt='loader'/>
            </div>
          </div>) : ""} 


          <div className="login-form">
            
          {phase === "SUCCESS" && user && user.code && user.code ===400 ? (
          <div className="alert alert-danger" role="alert">
              Invalid User credentials!
          </div>) : ""} 

                        <div className="form-group">
                            <InputField
                            field="email"
                            label="Email*"
                            value={email || ''}
                            className="form-control" placeholder="Email"
                            onChange={this.onChange}/>
                        </div>
                        <div className="form-group">
                            <InputField  
                            field="password"
                            label="Password*"
                            value={password || ''}
                            type="password"
                            onChange={this.onChange} 
                            onKeyPress={(e) => 
                              (e.key === 'Enter') ? this.loginUser() : '' 
                            }
                            className="form-control" placeholder="Password"/>
                        </div>
                        <ButtonField className="btn green-btn btn-flat m-b-30 m-t-30" type="submit" label="Login" onClick={this.loginUser}  />
        
                </div>
                
            </div>
      </div>
      </div>
      
      </>
    );
  }
}

export default Login;
