import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import loader from '../../images/loader.gif';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InputField from '../../component/inputField';
import { getUsersInfo, addUserInfo, editUserInfo } from './Store';
class UserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
           first_name:'',
            last_name:'',
            email:'',
            old_password:'',
            new_password:'',
            confirm_password:'',
            userId: '',
            role: '',
            userInfoData:'',
            addUserError:'',
            addPasswordError:'',
            edit_user: 0,
            edit_pwd : 0,
            textFilter:'',      
            searchJson: {
                "draw": 3,
                "columns": [{
                    "data": "id"
                }],
                "order": [{
                    "column": 0,
                    "dir": "desc"
                }],
                "start": 0,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false,
                    "from_date":"",
                    "to_date":""
                }
            }
        };
    }

    notify = (message) => toast.success(message);
    errorNotify = (message) => toast.error(message);
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    componentDidMount() {
        const {LoginStore: {user}} = this.props;
        if(user && user.details) {
            this.setState({
                first_name: user.details.first_name,
                last_name: (user.details.last_name)?user.details.last_name:'',
                email: user.details.email,
                userId : user.details.id
            })
        }
        
    }
    
    submitUserRequest = () => {
        const { editUserInfo, LoginStore: {user} } = this.props;
        const { first_name, last_name, userId} = this.state;
        const {isValid, error } = this.validateForm();
        if( isValid === true ) {
            user.details.first_name = first_name;
            user.details.last_name = last_name;
           editUserInfo({ first_name, last_name, userId });
           this.setState({edit_user: 1, edit_pwd: 0});
        } else { 
            this.setState({addUserError: error});
        }
    }
    submitPasswordRequest = async () => {
        const { editUserInfo } = this.props;
        const { new_password, old_password, userId} = this.state;
        const {isValid, error } = this.validatePassword();
        if( isValid === true ) {
            this.setState({addPasswordError: {}});
            await editUserInfo({new_password, userId, old_password });
            this.setState({old_password:'',
            new_password: '',
             confirm_password:'',
             edit_user: 0,
             edit_pwd: 1});
        } else { 
           
            this.setState({addPasswordError: error});
        }
    }
    validateForm = () => {
        const { first_name, last_name, email, password, userId } = this.state
        let error = {} 
        if (first_name === "") {
            error.first_name ="First name is required!"
        }
        if (last_name === "") {
            error.last_name ="Last name is required!"
        }
        if (email === "") {
            error.email ="Email is required!"
        }

        if (userId==="" && password === "") {
            error.password ="Password is required!"
        }
        return {
         isValid: Object.keys(error).length === 0 ,
         error
        }
    }
    validatePassword = () => {
        const { new_password, confirm_password, old_password } = this.state
        let error = {} 
        if (old_password === "") {
            error.old_password ="Old password is required!"
        }
        if (new_password === "") {
            error.new_password ="New password is required!"
        }
        if (confirm_password === "") {
            error.confirm_password ="Confirm password is required!"
        }
        if (confirm_password !== new_password) {
            error.confirm_password ="Confirm password does not match with new password!"
        }
        return {
         isValid: Object.keys(error).length === 0 ,
         error
        }
    }
    backtohome= () => {
       const {history} = this.props;
        history.push('/dashboard');

    }
    static getDerivedStateFromProps(props, prevSate) {
        const { UsersStore:{editUser}} = props;
        const { editUserData, edit_pwd, edit_user} = prevSate
        if (editUser !== editUserData) {
            if(editUser && editUser.code  === 400) {
                toast.error("Incorrect Old Password!");
            } 
            if(editUser && editUser.code  === 200 && edit_pwd === 1) {
                toast.success("Password updated successfully!");
            }
            if(editUser && editUser.code  === 200 && edit_user === 1) {
                toast.success("Account details updated successfully!");
            }
            return {
                editUserData: editUser
            }
        }
        return null;
    }
    render() {
        const { UsersStore:{ phase, editUser}} = this.props;
        const { email, first_name, last_name, addUserError,
             new_password, confirm_password, old_password, addPasswordError} = this.state;

        return (
            <>
        <div id="left-panel" className="left-panel">
            
        <div className="content">
            <div className="animated fadedIn">
            {(phase === "LOADING") ? (
          <div className='siteloader'><div className='siteloader-inner'>
            <img src={loader} alt='loader'/></div></div>
          ): ""} 
          {((editUser && editUser.code && editUser.code ===200) || 
          (editUser && editUser.code && editUser.code ===400)) ? <div><ToastContainer /></div>:''}
          

            <div className="row">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            <strong className="card-title">Manage User Profile</strong>
                        </div>
                        <div className="card-body">
                        { ((addUserError.first_name !=="" &&  addUserError.first_name!==undefined) ||
                        (addUserError.last_name !=="" &&  addUserError.last_name!==undefined))?(
                            <div className="col-md-12">
                                <div className="alert alert-danger" role="alert">
                                    <p>{addUserError.first_name}</p>
                                    <p>{addUserError.last_name}</p>
                                </div>
                            </div>
                            )
                            :"" }
                            <div className="col-md-12 mt-3">
                                <InputField
                                field="email"
                                label="Email*"
                                value={email || ''}
                                className="form-control" placeholder="Email"
                                onChange={this.onChange} disabled/>
                            </div>
                           
                            <div className="col-md-12 mt-3">
                            <InputField
                            field="first_name"
                            label="First Name*"
                            value={first_name || ''}
                            className="form-control" placeholder="First Name"
                            onChange={this.onChange}/>
                            </div>
                            <div className="col-md-12 mt-3">
                            <InputField
                            field="last_name"
                            label="Last Name*"
                            value={last_name || ''}
                            className="form-control" placeholder="Last Name"
                            onChange={this.onChange}/>
                            </div>
                            <div className="col-md-12 mt-3">
                            <div className="form-actions form-group">   
                                <Button type="button" onClick={this.backtohome} className="btn btn-secondary btn-margin">Cancel</Button>
                                <Button type="button" onClick={() => this.submitUserRequest()} className="btn btn-success btn-margin">Save</Button>
                            </div>
                            </div>
                    </div> 
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            <strong className="card-title">Change Password</strong>
                        </div>
                        <div className="card-body">
                        
                        { ((addPasswordError.old_password !=="" &&  addPasswordError.old_password!==undefined) ||
                            (addPasswordError.new_password !=="" &&  addPasswordError.new_password!==undefined) ||
                            (addPasswordError.confirm_password !=="" &&  addPasswordError.confirm_password!==undefined))?(
                            <div className="col-md-12">
                                <div className="alert alert-danger" role="alert">
                                    <p>{addPasswordError.old_password}</p>
                                    <p>{addPasswordError.new_password}</p>
                                    <p>{addPasswordError.confirm_password}</p>
                                </div>
                            </div>
                            )
                            :"" }
                           <div className="col-md-12 mt-3">
                            <InputField  
                                field="old_password"
                                label="Old Password*"
                                value={old_password||""}
                                type="password"
                                onChange={this.onChange} 
                                className="form-control" placeholder="Password" />
                            </div>
                            <div className="col-md-12 mt-3">
                            <InputField  
                                field="new_password"
                                label="New Password*"
                                value={new_password||""}
                                type="password"
                                onChange={this.onChange} 
                                className="form-control" placeholder="Password" />
                            </div>
                            <div className="col-md-12 mt-3">
                            <InputField  
                                field="confirm_password"
                                label="Confirm Password*"
                                value={confirm_password||""}
                                type="password"
                                onChange={this.onChange}
                                onKeyPress={(e) => (e.key === 'Enter') ? this.submitPasswordRequest() : ''}
                                className="form-control" placeholder="Password" />
                            </div>
                            <div className="col-md-12 mt-3">
                                <div className="form-actions form-group">   
                                    <Button type="button" onClick={this.backtohome} className="btn btn-secondary btn-margin">Cancel</Button>
                                    <Button type="button" onClick={() => this.submitPasswordRequest()} className="btn btn-success btn-margin">Save</Button>
                                </div>
                            </div>
                    </div> 
                    </div>
                </div>
           
            </div>
        </div>
    </div>
    </div>
    </>
        );
    }
} 

const mapStateToProps = DashboardStore => DashboardStore;
const mapDispatchToProps = {
    getUsersInfo: (params) => getUsersInfo(params),
    addUserInfo:(params) => addUserInfo(params),
    editUserInfo:(params)=> editUserInfo(params)
};
export default connect(mapStateToProps, mapDispatchToProps)(UserSettings);
