import { takeLatest, call, put } from 'redux-saga/effects';
import {
  GET_USERS, GET_USERS_SUCCESS, GET_USERS_ERROR, 
  ADD_USERS, ADD_USERS_ERROR, ADD_USERS_SUCCESS,
  EDIT_USERS, EDIT_USERS_SUCCESS, EDIT_USERS_ERROR
} from './constant';

import { getUsersListApi, userRegisterApi, userUpdateApi  } from './api';

/**
 * action Type of GET_DASHBOARD_INFO
 * @param {Object} action
 */
function* getUserList(params) {
  try {
    const data  = yield call(getUsersListApi, params);
    yield put({ type: GET_USERS_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_USERS_ERROR, error });
  }
}
function* AddUserInfo(params) {
  try {
    const data  = yield call(userRegisterApi, params.params);
    yield put({ type: ADD_USERS_SUCCESS, data });
  } catch (error) {
    yield put({ type: ADD_USERS_ERROR, error });
  }
}
function* EditUserInfo(params) {
  try {
    const data  = yield call(userUpdateApi, params.params);
    yield put({ type: EDIT_USERS_SUCCESS, data });
  } catch (error) {
    yield put({ type: EDIT_USERS_ERROR, error });
  }
}

/**
 * Get Login Saga
 */
export function* manageUsersSaga() {
  yield takeLatest(GET_USERS, getUserList);
  yield takeLatest(ADD_USERS, AddUserInfo);
  yield takeLatest(EDIT_USERS, EditUserInfo);
}
