import {
  INIT, SUCCESS, LOADING, ERROR,
} from '../../../utils/constants';
import {
  GET_USERS, GET_USERS_SUCCESS, GET_USERS_ERROR,
  ADD_USERS_SUCCESS, ADD_USERS_ERROR, ADD_USERS,
  EDIT_USERS, EDIT_USERS_SUCCESS, EDIT_USERS_ERROR
} from './constant';


const initialState = {
  phase: INIT,
  usersList: null,
  error: null
};

/**
 * User Reducer
 * @param {Object} state
 * @param {Object} action
 */
export function UsersStore(state = initialState, action) {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        usersList: action.data,
        error: null,
      };
    case GET_USERS_ERROR:
      return { ...state, phase: ERROR, error: action.error };
    case ADD_USERS:
      return {
        ...state,
        phase: LOADING,
      };
    case ADD_USERS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        addUser: action.data,
        error: null,
      };
    case ADD_USERS_ERROR:
      return { ...state, phase: ERROR, error: action.error };
      case EDIT_USERS:
        return {
          ...state,
          phase: LOADING,
        };
      case EDIT_USERS_SUCCESS:
        return {
          ...state,
          phase: SUCCESS,
          editUser: action.data,
          error: null,
        };
      case EDIT_USERS_ERROR:
        return { ...state, phase: ERROR, error: action.error };
    default:
      return state;
  }
}
