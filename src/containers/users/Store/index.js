
import {
  GET_USERS, ADD_USERS, EDIT_USERS
} from './constant';

/**
 * Get Dashbord info
 */
export const getUsersInfo = (params) => ({
  type: GET_USERS, params
});

export const addUserInfo =(param) => ({
  type: ADD_USERS,
  params: param
})

export const editUserInfo =(param) => ({
  type: EDIT_USERS,
  params: param
})
