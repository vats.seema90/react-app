import axios from 'axios';
import { API_URL } from '../../../utils/constants';

export const getUsersListApi = async (param) => {
  try {
    const response =  await axios({
      method: 'POST',
      url: `${API_URL}all-users`,
      headers: { 'Content-Type': 'application/json' },
      data: param.params
    });
    return response
  } catch (e) {
    return [];
  }
};
export const userRegisterApi = async (postData) => {
  try {
    postData.role = (postData.role !=="")? postData.role: '1';
    return await axios({
      method: 'POST',
      url: `${API_URL}register`,
      data: {'email': postData.email, 'password': postData.password, 'first_name': postData.first_name, 'last_name': postData.last_name, role: postData.role},
      headers: {'Content-Type': 'application/json'},
    });
  } catch (e) {
    return e;
  }
};
export const userUpdateApi = async (postData) => {
  try {
    let paramsArr = {};
    if(postData.new_password && postData.old_password) {
      paramsArr.password = postData.new_password;
      paramsArr.old_password = postData.old_password;
      paramsArr.new_password = postData.new_password;
      paramsArr.user_id = postData.userId;
      const response = await axios({
        method: 'PUT',
        url: `${API_URL}password/update`,
        data: paramsArr,
        headers: {'Content-Type': 'application/json'},
      });
      return response;

    } else {
      paramsArr.user_id = postData.userId
      paramsArr.first_name = postData.first_name
      paramsArr.last_name = postData.last_name
      paramsArr.role = postData.role
      return await axios({
        method: 'PUT',
        url: `${API_URL}user/update`,
        data: paramsArr,
        headers: {'Content-Type': 'application/json'},
      });
    }
    
  } catch (e) {
    return e;
  }
};







