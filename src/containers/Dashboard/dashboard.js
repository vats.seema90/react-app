import React, { Component } from 'react';
import loader from '../../images/loader.gif';
import { TabContainer, Tabs, Tab, Table, Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import 'react-toastify/dist/ReactToastify.css';
import 'moment-timezone';
import axios from 'axios';
import { API_URL } from '../../utils/constants';
import Select from 'react-select';

/**
 * Dashboard component
 * It includes environment based application details.
 * Also, on version click, user can see commit details.
 */
class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabId: '9',
            subTab: '',
            activeTab: true,
            dashboradTabInfoData: [],
            programDetailData: [],
            selectedPrograms: [],
            activeRecord: '',
            showModal: false,
            last_updated_at: '',
            show_loader: false
        };
    }
    /**
     * On page load selected tab fetch program data.
     * On based of programdata fetch application details.
     */

    async componentDidMount() {
        const { tabId } = this.state
        const { getEnvironemntInfo, getSelectedTabApplicationTypeData } = this.props;

        await getEnvironemntInfo({page: 'dashboard'})
        getSelectedTabApplicationTypeData({ tabId });

    }

    /**
     * Function to get target status
     */
    async getInfraHealthApi() {

        const { getHealthCheckInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getHealthCheckInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async getAnchorStatusApi() {
        const { getAnchorStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getAnchorStatusInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async getInstanceStatusApi() {
        const { getInstanceStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getInstanceStatusInfo();
        }
    }

    logout = async () => {
        const { getLogout, history } = this.props;

        localStorage.clear();
        await getLogout();
        history.push('/');
    }
    /**
     * This function refresh content if not matches to previous stored data.
     * @param {*} props 
     * @param {*} prevSate 
     */

    static getDerivedStateFromProps(props, prevSate) {
        const { DashboardStore: { dashboradInfo } } = props;
        const { dashboradInfoData } = prevSate

        if (dashboradInfo !== dashboradInfoData) {
            return {
                dashboradInfoData: dashboradInfo,
            }
        }
        return null;
    }
    /**
     * This fmethod calls apis from saga based active tab just after page load.
     * Here default subtab is set and activeTab is made false.
     */
    componentDidUpdate() {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getDashboradProgramData } = this.props
        const { tabId, activeTab, dashboradTabInfoData, programDetailData } = this.state;

        if (dashboradTabInfo !== dashboradTabInfoData) {
            if (dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id) {

                if (activeTab) {
                    getDashboradProgramData({ tabId, subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id });
                }
            }
            this.setState({
                dashboradTabInfoData: dashboradTabInfo,
                subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id,
                activeTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id ? false : true
            })
            this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[1] && dashboradTabInfo[1].updated_at })

        }

        if (programDetailData !== programDetail) {

            this.setState({
                programDetailData: programDetail
            })
        }
    }
    /**
     * This is called for version updates and fetch application for active tab.
     */
    refreshVersionInfo = async (clicked) => {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getSelectedTabApplicationTypeData } = this.props
        const { tabId } = this.state;
        this.setState({ show_loader: true });
        const processDataAsycn = async () => {
            let url = `${API_URL}update-version-details`;
            let data = { env_id: (tabId === '8') ? 'versions' : tabId };

            const response = await axios({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'application/json' },
                data: data
            });
            return response;
        };
        processDataAsycn().then(async (data) => {

            if (data.code === 200) {

                await getSelectedTabApplicationTypeData({ tabId });
                this.setState({
                    dashboradTabInfoData: dashboradTabInfo,
                    programDetailData: programDetail
                })
                this.setState({ show_loader: false });
                this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].updated_at });
            }
        }).catch((error) => {
        });

    }
    /**
     * This is called for close model popup.
     */
    onCloseModal = () => {
        this.setState({
            showModal: false,
            activeRecord: ''
        })
    }
    /**
     * Render popup info on branch click from list
     * @param {*} list 
     * @param {*} applications 
     */
    renderPopupInfo = (list, applications) => {
        let commitURL = '';
        if (applications.details && applications.details.includes('git')) {
            commitURL = (list.app_type_name === 'CE' && applications.app_name === "CE-UI") ?
                'https://github.com/eRecyclingCorps/' + applications.app_name + '/'
                : (list.app_type_name === 'CS' && applications.app_name === 'HC-App') ?
                    'https://github.com/eRecyclingCorps/ceadmin-app/'
                    : (list.app_type_name === 'CS' && applications.app_name === 'IVS') ?
                        'https://github.com/eRecyclingCorps/imei-validation/'
                        : (list.app_type_name === 'CS' && applications.app_name === 'Pricing') ?
                            'https://github.com/eRecyclingCorps/pricing-service/'
                            : (list.app_type_name === 'CS' && applications.app_name === 'HASS') ?
                                'https://github.com/eRecyclingCorps/haas/'
                                : (list.app_type_name === 'CS' && applications.app_name === 'Notifier') ?
                                    'https://github.com/eRecyclingCorps/notification-service/'
                                    : (list.app_type_name === 'CS' && applications.app_name === 'HSS') ?
                                        'https://github.com/eRecyclingCorps/hyla-shipping-service/'
                                        : (list.app_type_name === 'CS' && applications.app_name === 'Dock_Receipt') ?
                                            'https://github.com/eRecyclingCorps/hyla-dock-receipt/'
                                            : (list.app_type_name === 'CS' && applications.app_name === 'Email_Service') ?
                                                'https://github.com/eRecyclingCorps/hyla-email-service/'
                                                : 'https://github.com/eRecyclingCorps/' + list.app_type_name + '-' + applications.app_name + '/'



        }
        return commitURL;

    }
    /**
     * Render Current url below branch for current label for corrsponding app name.
     * @param {*} list 
     * @param {*} applications 
     */
    renderCurrentURL = (list, applications) => {
        return (list.app_type_name === 'CE' && applications.app_name === "CE-UI") ?
            'https://github.com/eRecyclingCorps/' + applications.app_name + '/commits/' + applications.version
            : (list.app_type_name === 'CS' && applications.app_name === 'HC-App') ?
                'https://github.com/eRecyclingCorps/ceadmin-app/commits/' + applications.version
                : (list.app_type_name === 'CS' && applications.app_name === 'IVS') ?
                    'https://github.com/eRecyclingCorps/imei-validation/commits/' + applications.version
                    : (list.app_type_name === 'CS' && applications.app_name === 'Pricing') ?
                        'https://github.com/eRecyclingCorps/pricing-service/commits/' + applications.version
                        : (list.app_type_name === 'CS' && applications.app_name === 'HAAS') ?
                            'https://github.com/eRecyclingCorps/haas/commits/' + applications.version
                            : (list.app_type_name === 'CS' && applications.app_name === 'Notifier') ?
                                'https://github.com/eRecyclingCorps/notification-service/commits/' + applications.version
                                : (list.app_type_name === 'CS' && applications.app_name === 'HSS') ?
                                    'https://github.com/eRecyclingCorps/hyla-shipping-service/commits/' + applications.version
                                    : (list.app_type_name === 'CS' && applications.app_name === 'Dock_Receipt') ?
                                        'https://github.com/eRecyclingCorps/hyla-dock-receipt/commits/' + applications.version
                                        : (list.app_type_name === 'CS' && applications.app_name === 'Email_Service') ?
                                            'https://github.com/eRecyclingCorps/hyla-email-service/commits/' + applications.version
                                            : 'https://github.com/eRecyclingCorps/' + list.app_type_name + '-' + applications.app_name + '/commits/' + applications.version;
    }
    /**
     * Render current url name plate which will be dispalyed for corrsponding current url.
     * @param {*} applications 
     */
    renderCurrentURLDisplayName = (applications) => {
        return (applications.details && applications.details.includes('git')
            && JSON.parse(applications.details).git.commit.id.abbrev
        ) ?
            JSON.parse(applications.details).git.commit.id.abbrev :
            (
                applications.details && applications.details.includes('git')
                && typeof JSON.parse(applications.details).git.commit.id === 'string'
            ) ?
                (JSON.parse(applications.details).git.commit.id).substring(0, 6) :
                (
                    applications.details && applications.details.includes('git')
                    && JSON.parse(applications.details).git.commit.id.full
                ) ?
                    (JSON.parse(applications.details).git.commit.id.full).substring(0, 6) : '';
    }
    /**
     * Render latest url for speciific application in list.
     * @param {*} applications 
     */
    renderLatestURL(applications) {
        return (applications.details && applications.details !== null && applications.details !== undefined && applications.details.includes('git')
            && (applications.git.commit !== "") &&
            (
                (typeof JSON.parse(applications.details).git.commit.id === 'string'
                    && JSON.parse(applications.details).git.commit.id
                    && (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                )
                ||
                (JSON.parse(applications.details).git.commit.id.abbrev &&
                    (applications.git.commit).substring(0, 7) !== (JSON.parse(applications.details).git.commit.id.abbrev))
                ||
                (JSON.parse(applications.details).git.commit.id.full &&
                    (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))
            )
        ) ?
            <div className='app-spacing'>
                <span> Latest: </span>
                {(applications.git.commit).substring(0, 6)}
            </div>
            : '';
    }
    /**
     * Render Status for corrsponding application.
     * @param {*} applications 
     */
    renderStatus(applications) {
        return (applications.details && applications.details.includes('git') &&
            (
                (typeof JSON.parse(applications.details).git.commit.id === 'string'
                    && JSON.parse(applications.details).git.commit.id
                    && (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                )
                ||
                (JSON.parse(applications.details).git.commit.id.abbrev &&
                    (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id.abbrev))
                ||
                (JSON.parse(applications.details).git.commit.id.full &&
                    (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))

            )

        ) ?
            <div className="progress mb-2">
                <div className="progress-bar bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> </div>
            </div>
            : (applications.details && applications.details.includes('git') &&
                (
                    (typeof JSON.parse(applications.details).git.commit.id === 'string'
                        && JSON.parse(applications.details).git.commit.id
                        && (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                    )
                    ||
                    (JSON.parse(applications.details).git.commit.id.abbrev &&
                        (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.abbrev))
                    ||
                    (JSON.parse(applications.details).git.commit.id.full &&
                        (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))
                )
            ) ?

                <div className="progress mb-2">
                    <div className="progress-bar bg-danger" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> </div>
                </div>
                : '';
    }
    /**
    * Render environment for versions tab in tds for application.
    * @param {*} applications 
    */
    renderEnvironment(application) {
        return (application.environment_name) ? application.environment_name : '';
    }
    /**
     * Render table headings UI.
     * @param {*} list 
     * @param {*} applications 
     */
    renderTableHeader() {
        const { subTab } = this.state;

        if (subTab === 1 || subTab === '1') {
            return (
                <tr>
                    <th>Server</th>
                    <th>Program</th>
                    <th>App</th>
                    <th>B2B</th>
                    <th>B2BUI</th>
                    <th>CE-UI</th>
                    <th>CSR</th>
                    <th>Consumer</th>
                    <th>FN</th>
                    <th>QiS</th>
                    <th>Reports</th>
                    <th>SelfServe</th>
                    <th>Web</th>
                </tr>
            )

        } else if (subTab === '2') {
            return (
                <tr>
                    <th>Server</th>
                    <th>Program</th>
                    <th>App</th>
                    <th>Reports</th>
                    <th>Web</th>
                </tr>
            )

        } else if (subTab === '3') {
            return (
                <tr>
                    <th>Server</th>
                    <th>Program</th>
                    <th>CE-Search</th>
                    <th>CTB/HiG</th>
                    <th>Dock_Receipt</th>
                    <th>Email_Service</th>
                    <th>HAAS</th>
                    <th>HC-App</th>
                    <th>HSS</th>
                    <th>Hc-web</th>
                    <th>IVS</th>
                    <th>Notifier</th>
                    <th>Pricing</th>
                </tr>
            )

        }

    }



    /**
     * This part render html with dynamic content.
     */

    render() {
        const { tabId, subTab, programDetailData, activeRecord, showModal, last_updated_at, show_loader, selectedPrograms } = this.state
        let { DashboardStore: { dashboradInfo, dashboradTabInfo, phase, programDetailPhase },
            getDashboradProgramData, getSelectedTabApplicationTypeData } = this.props
        if (tabId === '9' && dashboradTabInfo) {
            //Remove CS from sub tab..
            let dashboradTabInfoArr = dashboradTabInfo.filter(e => e.name !== 'CS');
            dashboradTabInfo = dashboradTabInfoArr;
        }

        const programOption = [
            { value: 'AWS-ATT', label: 'AWS-ATT' },
            { value: 'VERIZON', label: 'VERIZON' },
            { value: 'CISCO', label: 'CISCO' },
            { value: 'TRACFONE', label: 'TRACFONE' },
            { value: 'TELUS', label: 'TELUS' },
            { value: 'WIND', label: 'WIND' },
            { value: 'Gen Canada/FIDO', label: 'Gen Canada/FIDO' },
            { value: 'KOODO', label: 'KOODO' },
            { value: 'WADV', label: 'WADV' },
            { value: 'TARGET', label: 'TARGET' },
            { value: 'USCC', label: 'USCC' },
            { value: 'SAMSUNG', label: 'SAMSUNG' },
            { value: 'BBY', label: 'BBY' },
            { value: 'DOCOMO', label: 'DOCOMO' },
            { value: 'NRET', label: 'NRET' },
        ];

        return (
            <>

                <div className="content dashboard">
                    <div id="left-panel" className="left-panel">
                        {
                            (tabId !== '9') ?
                                <button type="button" onClick={() => {
                                    this.refreshVersionInfo(true);
                                }} className="btn btn-success recent-updates mb-1 button-right">
                                    <i className="fa fa-refresh"></i>
                                </button> : ''
                        }

                        {(last_updated_at && tabId !== '9' && tabId !== 'TARGET GROUP' && tabId !== 'anchorstatus' && tabId !== 'instancestatus') ?
                            <button type="button" className="btn btn-success recent-updates mb-1 button-right">
                                <span>Recent activity: {last_updated_at}

                                </span>
                            </button> : ''}



                        <Modal
                            show={showModal}
                            size="lg"
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                            onHide={this.onCloseModal}
                            backdrop="static"
                        >
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title-vcenter">
                                    Version Details
                    </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card">
                                            <div className="card-body">
                                                <div className="col-md-6 mt-3 ">
                                                    <strong> {
                                                        (activeRecord.server) ? activeRecord.server + ' > ' +
                                                            activeRecord.app_type_name + ' > ' +
                                                            activeRecord.program_name + ' > ' +
                                                            activeRecord.app_name : ''
                                                    } </strong>
                                                </div>
                                                <div className="col-md-6 mt-3 "><label><strong> Branch Name: </strong></label>  <a target="_blank" rel="noopener noreferrer" href={activeRecord.commitURL + 'commits/' + activeRecord.branch} >{activeRecord.branch}</a></div>


                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="card">
                                            <div className="card-header">
                                                <strong >Current Commit </strong>
                                            </div>
                                            <div className="card-body">
                                                <div className="col-md-12 mt-3"><label><strong> Commit: </strong></label>
                                                    <a target="_blank" rel="noopener noreferrer" title="Click here to view commit details." href={activeRecord.commitURL + 'commit/' + activeRecord.commit_full_name} >

                                                        {activeRecord.commit_full_name}
                                                    </a>
                                                </div>
                                                <div className="col-md-12 mt-3"><label><strong> Author: </strong></label> {activeRecord.build_user_name}</div>

                                                <div className="col-md-12 mt-3"><label><strong> Committed On: </strong></label> {activeRecord.commit_time} </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="card">
                                            <div className="card-header">
                                                <strong>Latest Commit </strong>

                                            </div>
                                            <div className="card-body">

                                                {(activeRecord.git !== undefined && activeRecord.git.commit !== '') ? <div className="col-md-12 mt-3"> <label><strong> Commit: </strong></label>
                                                    <a target="_blank" rel="noopener noreferrer" title="Click here to view latest commit details." href={activeRecord.commitURL + 'commit/' + activeRecord.git.commit} >
                                                        {activeRecord.git.commit.substring(0, 7)}</a></div> : ''}
                                                {(activeRecord.git !== undefined && activeRecord.git.author !== '') ?
                                                    <div className="col-md-12 mt-3"> <label><strong> Author: </strong></label>{activeRecord.git.author} </div> : ''}
                                                {(activeRecord.git !== undefined && activeRecord.git.date !== '') ?
                                                    <div className="col-md-12 mt-3"> <label><strong> Committed On: </strong></label>{activeRecord.git.date}</div> : ''}

                                                {(activeRecord.git === undefined || (activeRecord.git.commit === '' && activeRecord.git.author === '' && activeRecord.git.date === '')) ? 'No latest updates available' : ''}

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                            </Modal.Footer>
                        </Modal>

                        <TabContainer>
                            {show_loader === true || programDetailPhase === "LOADING" ? (<div className='siteloader'><div className='siteloader-inner'><img src={loader} alt='loader' /></div></div>) : ""}

                            <Tabs id="controlled-tab-example" activeKey={tabId} onSelect={(tabId) => {
                                this.setState({
                                    tabId
                                }, async () => {
                                    await getSelectedTabApplicationTypeData({ tabId })
                                })
                            }}>

                                {dashboradInfo && dashboradInfo.map((item) => (
                                    <Tab key={item.id} eventKey={item.id} title={item.name}>

                                        <Tabs key={item.id} id="controlled-tab-example" activeKey={subTab}
                                            onSelect={(subTab) => {
                                                this.setState({
                                                    subTab, selectedPrograms: []
                                                }, () => {
                                                    getDashboradProgramData({ tabId, subTab });
                                                })
                                            }}>

                                            {dashboradTabInfo && dashboradTabInfo.map((tablist) => (
                                                <Tab key={'main_tab_' + tablist.id} eventKey={tablist.id} title={tablist.name}>
                                                    {
                                                        (tabId === '9' && subTab !== '3') ?

                                                            <div className="row">
                                                                <div className="col-lg-12">
                                                                    <div className="card">
                                                                        <div className="card-body card-block">
                                                                            <div className="row form-group">
                                                                                <div className="col col-md-3">
                                                                                    <Select
                                                                                        name="colors"
                                                                                        value={selectedPrograms || []}
                                                                                        onChange={(value) => {
                                                                                            this.setState({
                                                                                                selectedPrograms: value ? value : []
                                                                                            })
                                                                                        }}
                                                                                        options={programOption}
                                                                                        className="basic-multi-select"
                                                                                        placeholder="Select a Program"
                                                                                        classNamePrefix="Program"
                                                                                    />

                                                                                </div>
                                                                                <div className="col col-md-6">
                                                                                    <button type="button" onClick={() => getDashboradProgramData({ tabId, subTab, selectedPrograms })} className="btn btn-primary">GO</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            : ''
                                                    }

<Table striped bordered hover>
                                                        <thead>
                                                            {this.renderTableHeader()}
                                                        </thead>
                                                        {(Number(tabId) === 10)?
                                                        <tbody>
                                                        {(programDetailData && programDetailData.length > 0) ? programDetailData.map((list, index) => (
                                                            <tr key={'program_' + index}>
                                                                <td> {list.server} </td>
                                                                <td> {list.program_name} </td>
                                                                {
                                                                Object.entries(list.customApplications).map(([key, value], obIndex) => (

                                                                    <td key={key + '_' + index}>
                                                                        {
                                                                        programDetailData[index].customApplications[key].map((application, applicationIndex) => (
                                                                        <div key={'div_'+key + '_' + index}>
                                                                            
                                                                           <span>Cloudformation Template:  {application.CFN_Template}</span> <br/>
                                                                           <span>Pipeline:  {application.pipeline}</span> 
                                                                          
                                                                        </div>
                                                                        ))}
                                                                    </td>))
                                                                }
                                                            </tr>
                                                        )) : phase === "SUCCESS" ? (
                                                            <tr><td colSpan={(subTab === 1 || subTab === '1') ? '13' : (subTab === '2') ? '5' : (subTab === '3') ? '13' : ''}>No Record Found!</td></tr>
                                                        ) : <tr><td colSpan="6"></td></tr>}

                                                    </tbody>:
                                                    <tbody>
                                                    {(programDetailData && programDetailData.length > 0) ? programDetailData.map((list, index) => (
                                                        <tr key={'program_' + index}>
                                                            <td> {list.server} </td>
                                                            <td> {list.program_name} </td>
                                                            {Object.entries(programDetailData[index].customApplications).map(([key, value], obIndex) => (

                                                                <td key={key + '_' + index}>
                                                                    {
                                                                        programDetailData[index].customApplications[key].map((application, applicationIndex) => (

                                                                            (application.details && application.details.includes('git')) ?

                                                                                <div className="app-spacing" key={'p' + applicationIndex + '_' + index}>

                                                                                    <OverlayTrigger overlay={(<Tooltip>{
                                                                                        (application.details && application.details.includes('git')) ?
                                                                                            'Click here for version details' : 'No info available!'}</Tooltip>)} placement="bottom">
                                                                                        <div onClick={async () => {
                                                                                            const versionDetails = JSON.parse(application.details);
                                                                                            const commitURL = await this.renderPopupInfo(list, application);
                                                                                            this.setState({
                                                                                                activeRecord: {
                                                                                                    'build_user_name': (versionDetails.git.build && versionDetails.git.build.user) ? versionDetails.git.build.user.name : '',
                                                                                                    'build_user_email': (versionDetails.git.build && versionDetails.git.build.user) ? versionDetails.git.build.user.email : '',
                                                                                                    'commit_short_name': (versionDetails.git.commit.id && versionDetails.git.commit.id.abbrev) ? versionDetails.git.commit.id.abbrev : '',
                                                                                                    'commit_full_name': (versionDetails.git.commit && typeof versionDetails.git.commit.id === 'string') ? (versionDetails.git.commit.id).substring(0, 7) : (versionDetails.git.commit.id.full).substring(0, 7),
                                                                                                    'commit_time': (versionDetails.git.commit) ? versionDetails.git.commit.time : '',
                                                                                                    'branch': versionDetails.git.branch,
                                                                                                    'server': list.server,
                                                                                                    'app_type_name': list.app_type_name,
                                                                                                    'program_name': list.program_name,
                                                                                                    'version': application.version,
                                                                                                    'app_name': application.app_name,
                                                                                                    'commitURL': commitURL,
                                                                                                    'git': application.git
                                                                                                },
                                                                                                showModal: true
                                                                                            })

                                                                                        }}>
                                                                                            {(application.version && application.environment_name) ? <span>{this.renderEnvironment(application)}: {application.version}</span> : ''}
                                                                                            {(application.version && !application.environment_name) ? <span>Branch: {application.version}</span> : ''}

                                                                                        </div>
                                                                                    </OverlayTrigger>
                                                                                    {(tabId !== '8') ?
                                                                                        <a target="_blank" key={applicationIndex + '_' + index} rel="noopener noreferrer" href={this.renderCurrentURL(list, application)}>
                                                                                            <span> Current: </span>
                                                                                            {this.renderCurrentURLDisplayName(application)}
                                                                                        </a>
                                                                                        : ''} 
                                                                                {(tabId !== '8') ?
                                                                                this.renderLatestURL(application) 
                                                                                : ''}
                                                                                <div className='app-spacing'>{this.renderStatus(application)}</div>
                                                                                </div> : ''))
                                                                    }
                                                                </td>))}
                                                        </tr>
                                                    )) : phase === "SUCCESS" ? (
                                                        <tr><td colSpan={(subTab === 1 || subTab === '1') ? '13' : (subTab === '2') ? '5' : (subTab === '3') ? '13' : ''}>No Record Found!</td></tr>
                                                    ) : <tr><td colSpan="6"></td></tr>}

                                                </tbody>}
                                                    </Table>
                                                </Tab>

                                            ))}

                                        </Tabs>
                                    </Tab>
                                ))}
                            
                            </Tabs>
                        </TabContainer>
                    </div>
                </div>
            </>
        );
    }
}

export default Dashboard;
