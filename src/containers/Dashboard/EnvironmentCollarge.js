import React, { Component } from 'react';
import loader from '../../images/loader.gif';
import { TabContainer, Tabs, Tab, Table, Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import 'react-toastify/dist/ReactToastify.css';
import 'moment-timezone';
import axios from 'axios';
import { API_URL } from '../../utils/constants';
import { connect } from 'react-redux';
import { getEnvironemntInfo, getDashboradProgramData,
     getSelectedTabApplicationTypeData, addDploymentRequestInfo,
     getHealthCheckInfo, getAnchorStatusInfo, getInstanceStatusInfo } from './Store';
import {getLogout} from '../Login/Store';
/**
 * Dashboard component
 * It includes environment based application details.
 * Also, on version click, user can see commit details.
 */
class EnvironmentCollarge extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabId: 2,
            subTab: '',
            activeTab: true,
            dashboradTabInfoData: [],
            programDetailData: [],
            activeRecord: '',
            showModal: false,
            last_updated_at: '',
            show_loader: false
        };
    }
    /**
     * On page load selected tab fetch program data.
     * On based of programdata fetch application details.
     */

    async componentDidMount() {
        const { tabId } = this.state
        const { getEnvironemntInfo, getSelectedTabApplicationTypeData } = this.props;

        await getEnvironemntInfo()
        await getSelectedTabApplicationTypeData({ tabId });

    }

    /**
     * Function to get target status
     */
    async  getInfraHealthApi() {

        const { getHealthCheckInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getHealthCheckInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async  getAnchorStatusApi() {
        const { getAnchorStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getAnchorStatusInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async  getInstanceStatusApi() {
        const { getInstanceStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getInstanceStatusInfo();
        }
    }

    logout = async () => {
        const { getLogout, history } = this.props;

        localStorage.clear();
        await getLogout();
        history.push('/');
    }
    /**
     * This function refresh content if not matches to previous stored data.
     * @param {*} props 
     * @param {*} prevSate 
     */

    static getDerivedStateFromProps(props, prevSate) {
        const { DashboardStore: { dashboradInfo } } = props;
        const { dashboradInfoData } = prevSate

        if (dashboradInfo !== dashboradInfoData) {
            return {
                dashboradInfoData: dashboradInfo,
            }
        }
        return null;
    }
    /**
     * This fmethod calls apis from saga based active tab just after page load.
     * Here default subtab is set and activeTab is made false.
     */
    componentDidUpdate() {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getDashboradProgramData } = this.props
        const { tabId, activeTab, dashboradTabInfoData, programDetailData } = this.state;

        if (dashboradTabInfo !== dashboradTabInfoData) {
            if (dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id) {
                if (activeTab) {
                    getDashboradProgramData({ tabId, subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id });
                }
            }
            this.setState({
                dashboradTabInfoData: dashboradTabInfo,
                subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id,
                activeTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id ? false : true
            })
            this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[1] && dashboradTabInfo[1].updated_at })

        }

        if (programDetailData !== programDetail) {

            this.setState({
                programDetailData: programDetail
            })
        }
    }
    /**
     * This is called for version updates and fetch application for active tab.
     */
    refreshVersionInfo = async (clicked) => {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getSelectedTabApplicationTypeData } = this.props
        const { tabId } = this.state;
        this.setState({ show_loader: true });
        const processDataAsycn = async () => {

            const response = await axios({
                method: 'POST',
                url: `${API_URL}update-version-details`,
                headers: { 'Content-Type': 'application/json' },
                data: { env_id: tabId }
            });
            return response;
        };
        processDataAsycn().then(async (data) => {

            if (data.code === 200) {

                await getSelectedTabApplicationTypeData({ tabId });
                this.setState({
                    dashboradTabInfoData: dashboradTabInfo,
                    programDetailData: programDetail
                })
                this.setState({ show_loader: false });
                this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].updated_at });
            }
        }).catch((error) => {
        });

    }
    /**
     * This is called for close model popup.
     */
    onCloseModal = () => {
        this.setState({
            showModal: false,
            activeRecord: ''
        })
    }

    /**
     * This part render html with dynamic content.
     */

    render() {
        const { tabId, subTab, programDetailData, activeRecord, showModal, show_loader } = this.state
        const { DashboardStore: { dashboradInfo, programDetail, dashboradTabInfo, phase,  programDetailPhase },
            getDashboradProgramData, getSelectedTabApplicationTypeData } = this.props;
        const envCollarge =[];
        if (dashboradInfo) {
            for (const item of dashboradInfo) {
                if(item.id === 2 || item.id ===3 || item.id ===4) {
                    envCollarge.push(item);
                }
            }
        }
        
                

        return (
            <>
                <div className="content">
                    <div id="left-panel" className="left-panel">

                        <Modal
                            show={showModal}
                            size="lg"
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                            onHide={this.onCloseModal}
                            backdrop="static"
                        >
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title-vcenter">
                                    Version Details
                    </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card">
                                            <div className="card-body">
                                                <div className="col-md-6 mt-3 ">
                                                    <strong> {
                                                        (activeRecord.server) ? activeRecord.server + ' > ' +
                                                            activeRecord.app_type_name + ' > ' +
                                                            activeRecord.program_name + ' > ' +
                                                            activeRecord.app_name : ''
                                                    } </strong>
                                                </div>
                                                <div className="col-md-6 mt-3 "><label><strong> Branch Name: </strong></label>  <a target="_blank" rel="noopener noreferrer" href={activeRecord.commitURL + 'commits/' + activeRecord.branch} >{activeRecord.branch}</a></div>


                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="card">
                                            <div className="card-header">
                                                <strong >Current Commit </strong>
                                            </div>
                                            <div className="card-body">
                                                <div className="col-md-12 mt-3"><label><strong> Commit: </strong></label>
                                                    <a target="_blank" rel="noopener noreferrer" title="Click here to view commit details." href={activeRecord.commitURL + 'commit/' + activeRecord.commit_full_name} >

                                                        {activeRecord.commit_full_name}
                                                    </a>
                                                </div>
                                                <div className="col-md-12 mt-3"><label><strong> Author: </strong></label> {activeRecord.build_user_name}</div>

                                                <div className="col-md-12 mt-3"><label><strong> Committed On: </strong></label> {activeRecord.commit_time} </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="card">
                                            <div className="card-header">
                                                <strong>Latest Commit </strong>

                                            </div>
                                            <div className="card-body">

                                                {(activeRecord.git !== undefined && activeRecord.git.commit !== '') ? <div className="col-md-12 mt-3"> <label><strong> Commit: </strong></label>
                                                    <a target="_blank" rel="noopener noreferrer" title="Click here to view latest commit details." href={activeRecord.commitURL + 'commit/' + activeRecord.git.commit} >
                                                        {activeRecord.git.commit.substring(0, 7)}</a></div> : ''}
                                                {(activeRecord.git !== undefined && activeRecord.git.author !== '') ?
                                                    <div className="col-md-12 mt-3"> <label><strong> Author: </strong></label>{activeRecord.git.author} </div> : ''}
                                                {(activeRecord.git !== undefined && activeRecord.git.date !== '') ?
                                                    <div className="col-md-12 mt-3"> <label><strong> Committed On: </strong></label>{activeRecord.git.date}</div> : ''}

                                                {(activeRecord.git === undefined || (activeRecord.git.commit === '' && activeRecord.git.author === '' && activeRecord.git.date === '')) ? 'No latest updates available' : ''}

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                            </Modal.Footer>
                        </Modal>

                        <TabContainer>
                            {show_loader === true ||  programDetailPhase === "LOADING" || phase === "LOADING" ? (<div className='siteloader'><div className='siteloader-inner'><img src={loader} alt='loader' /></div></div>) : ""}

                            <Tabs id="controlled-tab-example" activeKey={tabId} onSelect={(tabId) => {
                                this.setState({
                                    tabId
                                }, () => {
                                    getSelectedTabApplicationTypeData({ tabId });
                                })
                            }}>
                                {envCollarge && envCollarge.map((item) => (
                                    <Tab key={item.id} eventKey={item.id} title={item.name}>

                                        <Tabs key={item.id} id="controlled-tab-example" activeKey={subTab} onSelect={(subTab) => {

                                            this.setState({
                                                subTab
                                            }, () => {
                                                getDashboradProgramData({ tabId, subTab });
                                            })
                                        }}>

                                            {dashboradTabInfo && dashboradTabInfo.map((tablist) => (
                                                <Tab key={'main_tab_' + tablist.id} eventKey={tablist.id} title={tablist.name}>
                                                    <Table striped bordered hover>
                                                        <thead>
                                                            <tr>
                                                                <th>Server</th>
                                                                
                                                                <th>Program</th>
                                                                {programDetailData && programDetailData[0] !== undefined && programDetailData[0].applications && programDetailData[0].applications.map((applications) => (

                                                                    <th key={applications.app_name}>{applications.app_name}</th>
                                                                ))}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {(programDetailData && programDetailData.length > 0) ? programDetailData.map((list, index) => (
                                                                <tr key={'program_' + index}>
                                                                    <td> {list.server} </td>
                                                                    
                                                                    <td> {list.program_name} </td>

                                                                    {list.applications.map((applications, applicationIndex) =>
                                                                        <td key={applications.version_id} >

                                                                            <OverlayTrigger overlay={(<Tooltip>{
                                                                                (applications.details && applications.details.includes('git')) ?
                                                                                    'Click here for version details' : 'No info avilable!'}</Tooltip>)} placement="bottom">
                                                                                <div onClick={() => {
                                                                                    if (applications.details && applications.details.includes('git')) {
                                                                                        const versionDetails = JSON.parse(applications.details);
                                                                                        const commitURL = (list.app_type_name === 'CE' && applications.app_name === "CE-UI") ?
                                                                                            'https://github.com/eRecyclingCorps/' + applications.app_name + '/'
                                                                                            : (list.app_type_name === 'CS' && applications.app_name === 'HC-App') ?
                                                                                                'https://github.com/eRecyclingCorps/ceadmin-app/'
                                                                                                : (list.app_type_name === 'CS' && applications.app_name === 'IVS') ?
                                                                                                    'https://github.com/eRecyclingCorps/imei-validation/'
                                                                                                    : (list.app_type_name === 'CS' && applications.app_name === 'Pricing') ?
                                                                                                        'https://github.com/eRecyclingCorps/pricing-service/'
                                                                                                        : (list.app_type_name === 'CS' && applications.app_name === 'HASS') ?
                                                                                                            'https://github.com/eRecyclingCorps/haas/'
                                                                                                            : (list.app_type_name === 'CS' && applications.app_name === 'Notifier') ?
                                                                                                                'https://github.com/eRecyclingCorps/notification-service/'
                                                                                                                : (list.app_type_name === 'CS' && applications.app_name === 'HSS') ?
                                                                                                                    'https://github.com/eRecyclingCorps/hyla-shipping-service/'
                                                                                                                    : (list.app_type_name === 'CS' && applications.app_name === 'Dock_Receipt') ?
                                                                                                                        'https://github.com/eRecyclingCorps/hyla-dock-receipt/'
                                                                                                                        : (list.app_type_name === 'CS' && applications.app_name === 'Email_Service') ?
                                                                                                                            'https://github.com/eRecyclingCorps/hyla-email-service/'
                                                                                                                            : 'https://github.com/eRecyclingCorps/' + list.app_type_name + '-' + applications.app_name + '/'

                                                                                        this.setState({
                                                                                            activeRecord: {
                                                                                                'build_user_name': (versionDetails.git.build && versionDetails.git.build.user) ? versionDetails.git.build.user.name : '',
                                                                                                'build_user_email': (versionDetails.git.build && versionDetails.git.build.user) ? versionDetails.git.build.user.email : '',
                                                                                                'commit_short_name': (versionDetails.git.commit.id && versionDetails.git.commit.id.abbrev) ? versionDetails.git.commit.id.abbrev : '',
                                                                                                'commit_full_name': (versionDetails.git.commit && typeof versionDetails.git.commit.id === 'string') ? (versionDetails.git.commit.id).substring(0, 7) : (versionDetails.git.commit.id.full).substring(0, 7),
                                                                                                'commit_time': (versionDetails.git.commit) ? versionDetails.git.commit.time : '',
                                                                                                'branch': versionDetails.git.branch,
                                                                                                'server': list.server,
                                                                                                'app_type_name': list.app_type_name,
                                                                                                'program_name': list.program_name,
                                                                                                'version': applications.version,
                                                                                                'app_name': applications.app_name,
                                                                                                'commitURL': commitURL,
                                                                                                'git': applications.git
                                                                                            },
                                                                                            showModal: true
                                                                                        })

                                                                                    }

                                                                                }}>
                                                                                   
                                                                           
                                                                            {(applications.version)? <span>Branch: {applications.version}</span>:''}
                                                                            </div>
                                                                            </OverlayTrigger>

                                                                            {
                                                                                (applications.details && applications.details.includes('git'))
                                                                                    ?
                                                                                    <a target="_blank" rel="noopener noreferrer" href={(list.app_type_name === 'CE' && applications.app_name === "CE-UI") ?
                                                                                        'https://github.com/eRecyclingCorps/' + applications.app_name + '/commits/' + applications.version
                                                                                        : (list.app_type_name === 'CS' && applications.app_name === 'HC-App') ?
                                                                                            'https://github.com/eRecyclingCorps/ceadmin-app/commits/' + applications.version
                                                                                            : (list.app_type_name === 'CS' && applications.app_name === 'IVS') ?
                                                                                                'https://github.com/eRecyclingCorps/imei-validation/commits/' + applications.version
                                                                                                : (list.app_type_name === 'CS' && applications.app_name === 'Pricing') ?
                                                                                                    'https://github.com/eRecyclingCorps/pricing-service/commits/' + applications.version
                                                                                                    : (list.app_type_name === 'CS' && applications.app_name === 'HAAS') ?
                                                                                                        'https://github.com/eRecyclingCorps/haas/commits/' + applications.version
                                                                                                        : (list.app_type_name === 'CS' && applications.app_name === 'Notifier') ?
                                                                                                            'https://github.com/eRecyclingCorps/notification-service/commits/' + applications.version
                                                                                                            : (list.app_type_name === 'CS' && applications.app_name === 'HSS') ?
                                                                                                                'https://github.com/eRecyclingCorps/hyla-shipping-service/commits/' + applications.version
                                                                                                                : (list.app_type_name === 'CS' && applications.app_name === 'Dock_Receipt') ?
                                                                                                                    'https://github.com/eRecyclingCorps/hyla-dock-receipt/commits/' + applications.version
                                                                                                                    : (list.app_type_name === 'CS' && applications.app_name === 'Email_Service') ?
                                                                                                                        'https://github.com/eRecyclingCorps/hyla-email-service/commits/' + applications.version
                                                                                                                        : 'https://github.com/eRecyclingCorps/' + list.app_type_name + '-' + applications.app_name + '/commits/' + applications.version
                                                                                    }>
                                                                                         <span> Current: </span>


                                                                                        {(applications.details && applications.details.includes('git')
                                                                                            && JSON.parse(applications.details).git.commit.id.abbrev
                                                                                        ) ?
                                                                                            JSON.parse(applications.details).git.commit.id.abbrev :
                                                                                            (
                                                                                                applications.details && applications.details.includes('git')
                                                                                                && typeof JSON.parse(applications.details).git.commit.id === 'string'
                                                                                            ) ?
                                                                                            (JSON.parse(applications.details).git.commit.id).substring(0, 6) :
                                                                                                (
                                                                                                    applications.details && applications.details.includes('git')
                                                                                                    && JSON.parse(applications.details).git.commit.id.full
                                                                                                ) ?
                                                                                                (JSON.parse(applications.details).git.commit.id.full).substring(0, 6) : ''}
                                                                                    </a>
                                                                                    : ''

                                                                            }
                                                                            <br />


                                                                            {
                                                                                (applications.details && applications.details !== null && applications.details !== undefined && applications.details.includes('git')
                                                                                    && (applications.git.commit !== "") &&
                                                                                    (
                                                                                        (typeof JSON.parse(applications.details).git.commit.id === 'string'
                                                                                            && JSON.parse(applications.details).git.commit.id
                                                                                            && (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                                                                                        )
                                                                                        ||
                                                                                        (JSON.parse(applications.details).git.commit.id.abbrev &&
                                                                                            (applications.git.commit).substring(0, 7) !== (JSON.parse(applications.details).git.commit.id.abbrev))
                                                                                        ||
                                                                                        (JSON.parse(applications.details).git.commit.id.full &&
                                                                                            (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))
                                                                                    )
                                                                                ) ?

                                                                                    'Latest: ' + (applications.git.commit).substring(0, 6)
                                                                                    : ''
                                                                            }
                                                                            <br />


                                                                            {
                                                                                (applications.details && applications.details.includes('git') &&
                                                                                    (
                                                                                        (typeof JSON.parse(applications.details).git.commit.id === 'string'
                                                                                            && JSON.parse(applications.details).git.commit.id
                                                                                            && (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                                                                                        )
                                                                                        ||
                                                                                        (JSON.parse(applications.details).git.commit.id.abbrev &&
                                                                                            (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id.abbrev))
                                                                                        ||
                                                                                        (JSON.parse(applications.details).git.commit.id.full &&
                                                                                            (applications.git.commit).substring(0, 6) === (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))

                                                                                    )

                                                                                ) ?
                                                                                    <div className="progress mb-2">
                                                                                        <div className="progress-bar bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> </div>
                                                                                    </div>
                                                                                    : (applications.details && applications.details.includes('git') &&
                                                                                        (
                                                                                            (typeof JSON.parse(applications.details).git.commit.id === 'string'
                                                                                                && JSON.parse(applications.details).git.commit.id
                                                                                                && (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id).substring(0, 6)
                                                                                            )
                                                                                            ||
                                                                                            (JSON.parse(applications.details).git.commit.id.abbrev &&
                                                                                                (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.abbrev))
                                                                                            ||
                                                                                            (JSON.parse(applications.details).git.commit.id.full &&
                                                                                                (applications.git.commit).substring(0, 6) !== (JSON.parse(applications.details).git.commit.id.full).substring(0, 6))
                                                                                        )
                                                                                    ) ?

                                                                                        <div className="progress mb-2">
                                                                                            <div className="progress-bar bg-danger" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> </div>
                                                                                        </div>
                                                                                        : ''
                                                                            }


                                                                        </td>
                                                                    )}

                                                                </tr>
                                                            )) : phase === "SUCCESS" ? (
                                                                <tr>
                                                                    <td colSpan={`${3 + (programDetail && programDetail[0].applications ? programDetail[0].applications.length : 0)}`}>
                                                                        No Record Found!
                                                        </td>
                                                                </tr>) : <tr><td colSpan="6"></td></tr>}

                                                        </tbody>
                                                    </Table>

                                                </Tab>

                                            ))}

                                        </Tabs>
                                    </Tab>
                                ))}

                            </Tabs>
                        </TabContainer>
                    </div>
                </div>
            </>
        );
    }
}
const mapStateToProps = DashboardStore => DashboardStore;
/**
 * This object calls saga functions so can use them on dashboard.
 */
const mapDispatchToProps = {
    getEnvironemntInfo: () => getEnvironemntInfo(),
    getSelectedTabApplicationTypeData: (param) => getSelectedTabApplicationTypeData(param),
    getDashboradProgramData: (param) => getDashboradProgramData(param),
    addDploymentRequestInfo: (param) => addDploymentRequestInfo(param),
    getHealthCheckInfo: () => getHealthCheckInfo(),
    getAnchorStatusInfo: () => getAnchorStatusInfo(),
    getInstanceStatusInfo: () => getInstanceStatusInfo(),
    getLogout: () => getLogout()
};
export default connect(mapStateToProps, mapDispatchToProps)(EnvironmentCollarge);
