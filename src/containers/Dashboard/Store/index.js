
import {
  GET_ENVIRONMENT_INFO,
  GET_PROGRAME_DETAIL_INFO,
  GET_PROGRAME_TYPE_DETAIL_INFO,
  GET_APP_LIST_INFO,
  GET_APP_PROGRAM_LIST_INFO,
  ADD_DEPLOYMENT_REQUEST,
  GET_INFRA_HEALTH_CHECK,
  GET_ANCHOR_STATUS_CHECK,
  GET_INSTANCE_STATUS_CHECK
} from './constant';

/**
 * Get Environment info.
 */
export const getEnvironemntInfo = (param) => ({
  type: GET_ENVIRONMENT_INFO,param
});

/**
 * Get Dashbord program detail
 */
export const getDashboradProgramData = (param) => ({
  type: GET_PROGRAME_DETAIL_INFO, param
});

/**
 * Get Dashbord program tpye detail
 */
export const getSelectedTabApplicationTypeData = (param) => ({
  type: GET_PROGRAME_TYPE_DETAIL_INFO, param
});

/**
 * Get Dashbord applications detail
 */
export const getappdata = (param) => ({
  type: GET_APP_LIST_INFO, param
});

/**
 * Get application tpye detail
 * @param {*} param 
 */
export const getAppProgram = (param) => ({
  type: GET_APP_PROGRAM_LIST_INFO, param
});
/**
 * It sends Call to add deployment request info.
 * @param {*} param 
 */
export const addDploymentRequestInfo = (param) => ({
  type: ADD_DEPLOYMENT_REQUEST, param
});

/**
 *  It sends Call to fetch health check info
 */
export const getHealthCheckInfo = () => ({
  type: GET_INFRA_HEALTH_CHECK
});

/**
 *  It sends Call to fetch anchor check info
 */
export const getAnchorStatusInfo = () => ({
  type: GET_ANCHOR_STATUS_CHECK
});

/**
 *  It sends Call to fetch instance check info
 */
export const getInstanceStatusInfo = () => ({
  type: GET_INSTANCE_STATUS_CHECK
});
