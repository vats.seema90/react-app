import {
  INIT, SUCCESS, LOADING, ERROR,
} from '../../../utils/constants';
import {
  GET_ENVIRONMENT_INFO, GET_ENVIRONMENT_INFO_SUCCESS, GET_ENVIRONMENT_INFO_ERROR,
  GET_PROGRAME_DETAIL_INFO, GET_PROGRAME_DETAIL_INFO_SUCCESS, GET_PROGRAME_DETAIL_INFO_ERROR,
  GET_PROGRAME_TYPE_DETAIL_INFO, GET_PROGRAME_TYPE_DETAIL_INFO_SUCCESS, GET_PROGRAME_TYPE_DETAIL_INFO_ERROR,
  GET_APP_LIST_INFO, GET_APP_LIST_INFO_SUCCESS, GET_APP_LIST_INFO_ERROR,
  GET_APP_PROGRAM_LIST_INFO, GET_APP_PROGRAM_LIST_INFO_SUCCESS, GET_APP_PROGRAM_LIST_INFO_ERROR,
  ADD_DEPLOYMENT_REQUEST, ADD_DEPLOYMENT_REQUEST_SUCCESS, ADD_DEPLOYMENT_REQUEST_ERROR,
  GET_INFRA_HEALTH_CHECK, GET_INFRA_HEALTH_CHECK_SUCCESS, GET_INFRA_HEALTH_CHECK_ERROR,
  GET_ANCHOR_STATUS_CHECK, GET_ANCHOR_STATUS_CHECK_SUCCESS, GET_ANCHOR_STATUS_CHECK_ERROR,
  GET_INSTANCE_STATUS_CHECK, GET_INSTANCE_STATUS_CHECK_SUCCESS, GET_INSTANCE_STATUS_CHECK_ERROR
} from './constant';

/**
 * Intialize state for reponse params and sets them to be null.
 * @author hsandhu
 */
const initialState = {
  phase: INIT,
   programDetailPhase: INIT,
  dashboradInfo: null,
  programDetail: null,
  dashboradTabInfo: null,
  appProgramList: null,
  appList: null,
  error: null,
  addDeploymentRequest: null,
  healthCheck: null,
  instanceStatus: null,
  anchorStatus: null
};

/**
 * Dashboard reducer manage request and response based on state and actions.
 * @param {Object} state
 * @param {Object} action
 * @author hsandhu
 */
export function DashboardStore(state = initialState, action) {
  switch (action.type) {
    case GET_ENVIRONMENT_INFO:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_ENVIRONMENT_INFO_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        dashboradInfo: action.data,
        error: null,
      };
    case GET_ENVIRONMENT_INFO_ERROR:
      return { ...state, phase: ERROR, error: action.error };

    case GET_PROGRAME_DETAIL_INFO:
      return {
        ...state,
         programDetailPhase: LOADING,
        programDetail: null
      };
    case GET_PROGRAME_DETAIL_INFO_SUCCESS:
      return {
        ...state,
         programDetailPhase: SUCCESS,
        programDetail: action.data,
        error: null,
      };
    case GET_PROGRAME_DETAIL_INFO_ERROR:
      return { ...state,  programDetailPhase: ERROR, error: action.error };

    case GET_PROGRAME_TYPE_DETAIL_INFO:
      return {
        ...state,
        phase: LOADING,
        dashboradTabInfo: null,
      };
    case GET_PROGRAME_TYPE_DETAIL_INFO_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        dashboradTabInfo: action.data,
        error: null,
      };
    case GET_PROGRAME_TYPE_DETAIL_INFO_ERROR:
      return { ...state, phase: ERROR, error: action.error };


    case GET_APP_LIST_INFO:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_APP_LIST_INFO_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        appList: action.data,
        error: null,
      };
    case GET_APP_LIST_INFO_ERROR:
      return { ...state, phase: ERROR, appList: [], error: action.error };


    case GET_APP_PROGRAM_LIST_INFO:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_APP_PROGRAM_LIST_INFO_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        appProgramList: action.data,
        error: null,
      };
    case GET_APP_PROGRAM_LIST_INFO_ERROR:
      return { ...state, phase: ERROR, error: action.error };
    case ADD_DEPLOYMENT_REQUEST:
      return {
        ...state,
        phase: LOADING,
        addDeploymentRequest: null
      };
    case ADD_DEPLOYMENT_REQUEST_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        addDeploymentRequest: action.data,
        error: null,
      };
    case ADD_DEPLOYMENT_REQUEST_ERROR:
      return { ...state, phase: ERROR, error: action.error };

    case GET_INFRA_HEALTH_CHECK:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_INFRA_HEALTH_CHECK_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        healthCheck: action.data,
        error: null,
      };
    case GET_INFRA_HEALTH_CHECK_ERROR:
      return { ...state, phase: ERROR, error: action.error };

    case GET_ANCHOR_STATUS_CHECK:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_ANCHOR_STATUS_CHECK_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        anchorStatus: action.data,
        error: null,
      };
    case GET_ANCHOR_STATUS_CHECK_ERROR:
      return { ...state, phase: ERROR, error: action.error };


    case GET_INSTANCE_STATUS_CHECK:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_INSTANCE_STATUS_CHECK_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        instanceStatus: action.data,
        error: null,
      };
    case GET_INSTANCE_STATUS_CHECK_ERROR:
      return { ...state, phase: ERROR, error: action.error };
    default:
      return state;
  }
}
