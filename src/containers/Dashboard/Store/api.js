import axios from 'axios';
import { API_URL } from '../../../utils/constants';
import jsonData from'../../../component/data/deployments.json';
/**
 * Fetch all environments.
 * @param {*} param 
 * @author hsandhu
 */
export const getEnvironemntsApi = async (param) => {
  try {
    const response = await axios({
      method: 'GET',
      url: `${API_URL}get-environments`,
      headers: { 'Content-Type': 'application/json' },
    });
    console.log('param.page: ', param.param.page);
    if(param.param.page==='dashboard'){
      
      response.data.push({ id: 8, name: 'VERSIONS' })
      response.data.push({ id: 9, name: 'PROGRAMS' })
      let newArr1 =  response.data.filter(e => e.name !== 'STAGE');
      response.data  = newArr1 ;

    } else if(param.param.page === 'deployments'){
      
      let newArr1 =  response.data.filter(e => e.name !== 'STAGE');
      let newArr2 =  newArr1.filter(e => e.name !== 'QA-PROD');
      let newArr3 =  newArr2.filter(e => e.name !== 'PROD');
      response.data  = newArr3 ;
      
    }

    return response && response.data;
  } catch (e) {
    return [];
  }
};

/**
 * Api fetch program details for env, app_type_id
 * Incase versions, it collects data from another api
 * which is made conditional.
 * 
 * @param {*} param 
 * @author hsandhu
 */
export const getprogramDetailApi = async (param) => {
  try {
    let data = {
      "draw": 3,
      "columns": [
        {
          "data": "id"
        }
      ],
      "order": [
        {
          "column": 0,
          "dir": "desc"
        }
      ],
      "start": 0,
      "length": 500,
      "search": {
        "value": { "environment": "2", "application_type": "1" },
        "regex": false,
        "from_date": "",
        "to_date": ""
      }
    }

    data.search.value = { 'environment': param.tabId, 'application_type': param.subTab };
    let url = `${API_URL}get-applications`;
    let result = '', response=[];
   
  
    if (param.tabId === '8') {
      data.search.value  = { application_type: param.subTab }
      url = `${API_URL}get-all-applications`;
      response = await axios({
        method: 'POST',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        data: data
      });
    } else if(param.tabId === '9' && param.selectedPrograms.value) {
      data.search.value  =  {"application_type":param.subTab, "program_name": param.selectedPrograms.value}
      url = `${API_URL}get-selected-applications`;
      response = await axios({
        method: 'POST',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        data: data
      });
    } else if(param.page && param.page==='deployments' && Number(param.subTab) ===1) {
      response.data =jsonData["CE"];
    } else if(param.page && param.page==='deployments' && Number(param.subTab) ===2) {
      response.data =jsonData["CEW"];
    } else if(param.page && param.page==='deployments' && Number(param.subTab) ===3) {
      return {code:201, data:[], message: "ok"}
    } else if(param.page && param.page!=='deployments'){
      response = await axios({
        method: 'POST',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        data: data
      });

    }
    console.log(response);
    
    result = response;
  
    for (var i = 0; i < result.data.length; i++) {
      result.data[i].customApplications = await groupApplications(result.data[i].applications, param.subTab);
    };
   
    if (response.data.length === result.data.length) {
      return result && result.data
    }
  } catch (e) {
    return [];
  }
};
/**
 * This function generates group array for all envs for corrsponding program
 * So system can handle app_names which are not provided by backend.
 * 
 * @param {*} applications 
 * @param {*} subTab 
 */
export const groupApplications = async (applications, subTab) => {
  var categories = {}, groupBy = "app_name";


  let appNames = [];
  if (subTab === 1 || subTab === '1') {
    appNames = ['App', 'B2B', 'B2BUI', 'CE-UI', 'CSR', 'Consumer', 'FN', 'QiS', 'Reports', 'SelfServe', 'Web'];

  } else if (subTab === '2') {
    appNames = ['App', 'Reports', 'Web'];

  } else if (subTab === '3') {
    appNames = ['CE-Search', 'CTB/HiG', 'Dock_Receipt', 'Email_Service', 'HAAS', 'HC-App', 'HSS', 'Hc-web', 'IVS', 'Notifier', 'Pricing'];
  }
  for (let i = 0; i < appNames.length; i++) {
    categories[appNames[i]] = []
  }
  for (var j = 0; j < applications.length; j++) {
    categories[applications[j][groupBy]].push(applications[j]);

    categories[applications[j][groupBy]].sort((a, b) => {
      return a.environment_id - b.environment_id;
    });
  }

  return categories;
}

/**
 *  Api fetch program for env
 * @param {*} param
 * @author hsandhu 
 */
export const getSelectedTabApplicationTypeDataApi = async (param) => {
  try {
    if (param.tabId === '8' || param.tabId === '9' || param.tabId === '10') {
      param.tabId = 3;
    }
    console.log(param);

    const response = await axios({
      method: 'POST',
      url: `${API_URL}get-application-types`,
      headers: { 'Content-Type': 'application/json' },
      data: { 'env_id': Number(param.tabId) }
    });
    return response && response.data
  } catch (e) {
    return [];
  }
};
/**
 * Api fetch application for env, app_type_id
 * @param {*} param 
 * @author hsandhu
 */
export const getappdataApi = async (param) => {
  try {
    const response = await axios({
      method: 'POST',
      url: `${API_URL}get-app-data `,
      headers: { 'Content-Type': 'application/json' },
      data: { 'env_id': Number(param.env_id), app_type_id: Number(param.app_type_id) }
    });
    return response && response.data
  } catch (e) {
    return [];
  }
};
/**
 * Api fetch program data for env, app_type_name, app_type_id
 * @param {*} param 
 * @author hsandhu
 * 
 */
export const getappProgramdataApi = async (param) => {
  try {
    const response = await axios({
      method: 'POST',
      url: `${API_URL}get-program-data`,
      headers: { 'Content-Type': 'application/json' },
      data: { 'env_id': Number(param.env_id), app_type_id: Number(param.app_type_id), application_name: param.app_name }
    });
    return response && response.data
  } catch (e) {
    return [];
  }
};
/**
 * Api add deployment request version manually.
 * @param {*} param
 * @author hsandhu 
 */
export const addDploymentRequestInfoApi = async (param) => {
  try {
    const response = await axios({
      method: 'POST',
      url: `${API_URL}update-program-version`,
      headers: { 'Content-Type': 'application/json' },
      data: param
    });
    return response;
  } catch (e) {
    return [];
  }
}

/**
 * Api for fetch health check information
 * @param {*} param
 * @author hsandhu 
 */
export const getHealthCheckInfoApi = async () => {
  try {
    const response = await axios({
      method: 'GET',
      url: `https://o6go4m5b29.execute-api.us-east-1.amazonaws.com/default/tg_status`,
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': 'bqRfSjXgip4zFj4hhDssNR1AspAjAk36U331xZy7'
      }
    });
    return response;
  } catch (e) {
    return [];
  }
}

/**
 * Api for fetch anchor status information
 * @param {*} param
 * @author hsandhu 
 */
export const getAnchorStatusInfoApi = async () => {
  try {
    const response = await axios({
      method: 'GET',
      url: `https://pudcz4twcj.execute-api.us-east-1.amazonaws.com/default/anchor-status`,
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': 'bqRfSjXgip4zFj4hhDssNR1AspAjAk36U331xZy7'
      }
    });
    return response;
  } catch (e) {
    return [];
  }
}

/**
 * Api for fetch anchor status information
 * @param {*} param
 * @author hsandhu 
 */
export const getInstanceStatusInfoApi = async () => {
  try {
    const response = await axios({
      method: 'GET',
      url: `https://qc2f5vbqvi.execute-api.us-east-1.amazonaws.com/default/inst-status`,
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': 'bqRfSjXgip4zFj4hhDssNR1AspAjAk36U331xZy7'
      }
    });
    return response;
  } catch (e) {
    return [];
  }
}