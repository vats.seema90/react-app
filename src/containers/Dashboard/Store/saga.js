import { takeLatest, call, put } from 'redux-saga/effects';
import {
  GET_ENVIRONMENT_INFO, GET_ENVIRONMENT_INFO_SUCCESS, GET_ENVIRONMENT_INFO_ERROR,
  GET_PROGRAME_DETAIL_INFO, GET_PROGRAME_DETAIL_INFO_SUCCESS, GET_PROGRAME_DETAIL_INFO_ERROR,
  GET_PROGRAME_TYPE_DETAIL_INFO, GET_PROGRAME_TYPE_DETAIL_INFO_SUCCESS, GET_PROGRAME_TYPE_DETAIL_INFO_ERROR,
  GET_APP_LIST_INFO, GET_APP_LIST_INFO_SUCCESS, GET_APP_LIST_INFO_ERROR,
  GET_APP_PROGRAM_LIST_INFO, GET_APP_PROGRAM_LIST_INFO_SUCCESS, GET_APP_PROGRAM_LIST_INFO_ERROR,
  ADD_DEPLOYMENT_REQUEST, ADD_DEPLOYMENT_REQUEST_SUCCESS,ADD_DEPLOYMENT_REQUEST_ERROR,
  GET_INFRA_HEALTH_CHECK, GET_INFRA_HEALTH_CHECK_SUCCESS, GET_INFRA_HEALTH_CHECK_ERROR,
  GET_ANCHOR_STATUS_CHECK, GET_ANCHOR_STATUS_CHECK_SUCCESS, GET_ANCHOR_STATUS_CHECK_ERROR,
  GET_INSTANCE_STATUS_CHECK, GET_INSTANCE_STATUS_CHECK_SUCCESS, GET_INSTANCE_STATUS_CHECK_ERROR
} from './constant';

import { getEnvironemntsApi, getprogramDetailApi,
   getSelectedTabApplicationTypeDataApi, getappdataApi,
   getappProgramdataApi, addDploymentRequestInfoApi,
   getHealthCheckInfoApi, getAnchorStatusInfoApi, getInstanceStatusInfoApi } from './api';


/**
 * action Type of GET_ENVIRONMENT_INFO
 * @param {Object} action
 */
function* getEnvironemntInfo(param) {
  try {
   
    const data = yield call(getEnvironemntsApi, param);
    yield put({ type: GET_ENVIRONMENT_INFO_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_ENVIRONMENT_INFO_ERROR, error });
  }
}

/**
 * action Type of GET_PROGRAME_DETAIL_INFO
 * @param {Object} action
 */
function* getDashboradProgramData(action) {
  try {
    const data = yield call(getprogramDetailApi, action.param);
    yield put({ type: GET_PROGRAME_DETAIL_INFO_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_PROGRAME_DETAIL_INFO_ERROR, error });
  }
}

/**
 * action Type of GET_PROGRAME_TYPE_DETAIL
 * @param {Object} action
 */
function* getSelectedTabApplicationTypeData(action) {
  try {
    const data = yield call(getSelectedTabApplicationTypeDataApi, action.param);
    yield put({ type: GET_PROGRAME_TYPE_DETAIL_INFO_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_PROGRAME_TYPE_DETAIL_INFO_ERROR, error });
  }
}

/**
 * action Type of GET_APP_LIST_INFO
 * @param {Object} action
 */
function* getappdata(action) {
  try {
    const data = yield call(getappdataApi, action.param);
    yield put({ type: GET_APP_LIST_INFO_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_APP_LIST_INFO_ERROR, error });
  }
}

/**
 * action Type of GET_APP_PROGRAM_LIST_INFO
 * @param {Object} action
 */
function* getAppProgram(action) {
  try {
    const data = yield call(getappProgramdataApi, action.param);
    yield put({ type: GET_APP_PROGRAM_LIST_INFO_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_APP_PROGRAM_LIST_INFO_ERROR, error });
  }
}

/**
 * action Type of ADD_DEPLOYMENT_REQUEST
 * @param {Object} action
 */
function* addDploymentRequestInfo(params) {
  try {
    const data  = yield call(addDploymentRequestInfoApi, params.param);
    yield put({ type: ADD_DEPLOYMENT_REQUEST_SUCCESS, data });
  } catch (error) {
    yield put({ type: ADD_DEPLOYMENT_REQUEST_ERROR, error });
  }
}

/**
 * action Type of GET_HEALTH_CHECK
 * @param {Object} action
 */
function* getHealthCheckInfo() {
  try {
    const data = yield call(getHealthCheckInfoApi);
    yield put({ type: GET_INFRA_HEALTH_CHECK_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_INFRA_HEALTH_CHECK_ERROR, error });
  }
}

/**
 * action Type of GET_HEALTH_CHECK
 * @param {Object} action
 */
function* getAnchorStatusInfo() {
  try {
    const data = yield call(getAnchorStatusInfoApi);
    yield put({ type: GET_ANCHOR_STATUS_CHECK_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_ANCHOR_STATUS_CHECK_ERROR, error });
  }
}

/**
 * action Type of GET_HEALTH_CHECK
 * @param {Object} action
 */
function* getInstanceStatusInfo() {
  try {
    const data = yield call(getInstanceStatusInfoApi);
    yield put({ type: GET_INSTANCE_STATUS_CHECK_SUCCESS, data });
  } catch (error) {
    yield put({ type: GET_INSTANCE_STATUS_CHECK_ERROR, error });
  }
}

/**
 * Dashboard Saga manage all calls for all api request.
 */
export function* getDashboardSaga() {
  yield takeLatest(GET_ENVIRONMENT_INFO, getEnvironemntInfo);
  yield takeLatest(GET_PROGRAME_DETAIL_INFO, getDashboradProgramData);
  yield takeLatest(GET_PROGRAME_TYPE_DETAIL_INFO, getSelectedTabApplicationTypeData);
  yield takeLatest(GET_APP_LIST_INFO, getappdata);
  yield takeLatest(GET_APP_PROGRAM_LIST_INFO, getAppProgram);
  yield takeLatest(ADD_DEPLOYMENT_REQUEST, addDploymentRequestInfo);
  yield takeLatest(GET_INFRA_HEALTH_CHECK, getHealthCheckInfo);
  yield takeLatest(GET_ANCHOR_STATUS_CHECK, getAnchorStatusInfo);
  yield takeLatest(GET_INSTANCE_STATUS_CHECK, getInstanceStatusInfo);
}
