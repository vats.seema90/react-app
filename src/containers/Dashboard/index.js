import { connect } from 'react-redux';
import Dashboard from './dashboard';
import { getEnvironemntInfo, getDashboradProgramData,
     getSelectedTabApplicationTypeData, addDploymentRequestInfo,
     getHealthCheckInfo, getAnchorStatusInfo, getInstanceStatusInfo } from './Store';
import {getLogout} from '../../containers/Login/Store';

const mapStateToProps = DashboardStore => DashboardStore;
/**
 * This object calls saga functions so can use them on dashboard.
 */
const mapDispatchToProps = {
    getEnvironemntInfo: (param) => getEnvironemntInfo(param),
    getSelectedTabApplicationTypeData: (param) => getSelectedTabApplicationTypeData(param),
    getDashboradProgramData: (param) => getDashboradProgramData(param),
    addDploymentRequestInfo: (param) => addDploymentRequestInfo(param),
    getHealthCheckInfo: () => getHealthCheckInfo(),
    getAnchorStatusInfo: () => getAnchorStatusInfo(),
    getInstanceStatusInfo: () => getInstanceStatusInfo(),
    getLogout: () => getLogout()
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
