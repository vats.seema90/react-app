import React, { Component } from 'react';
import { connect } from 'react-redux';
import loader from '../../images/loader.gif';
import { TabContainer, Tabs, Tab, Table } from 'react-bootstrap';
import 'react-toastify/dist/ReactToastify.css';
import 'moment-timezone';
import axios from 'axios';
import { API_URL } from '../../utils/constants';

import { getEnvironemntInfo, getDashboradProgramData,
    getSelectedTabApplicationTypeData, addDploymentRequestInfo,
    getHealthCheckInfo, getAnchorStatusInfo, getInstanceStatusInfo } from './Store';
import {getLogout} from '../../containers/Login/Store';
/**
 * Dashboard component
 * It includes environment based application details.
 * Also, on version click, user can see commit details.
 */
class Infrastructure extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabId: 'TARGET GROUP',
            subTab: '',
            activeTab: true,
            dashboradTabInfoData: [],
            programDetailData: [],
            selectedPrograms: [],
            activeRecord: '',
            showModal: false,
            last_updated_at: '',
            show_loader: false
        };
    }
    /**
     * On page load selected tab fetch program data.
     * On based of programdata fetch application details.
     */

    async componentDidMount() {
        this.getInfraHealthApi();
    }

    /**
     * Function to get target status
     */
    async getInfraHealthApi() {

        const { getHealthCheckInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getHealthCheckInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async getAnchorStatusApi() {
        const { getAnchorStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getAnchorStatusInfo();
        }
    }

    /**
     * Function to get Anchor status
     */
    async getInstanceStatusApi() {
        const { getInstanceStatusInfo, LoginStore: { user } } = this.props;
        if (user.details.role_id === 1) {
            await getInstanceStatusInfo();
        }
    }

    logout = async () => {
        const { getLogout, history } = this.props;

        localStorage.clear();
        await getLogout();
        history.push('/');
    }
    /**
     * This function refresh content if not matches to previous stored data.
     * @param {*} props 
     * @param {*} prevSate 
     */

    static getDerivedStateFromProps(props, prevSate) {
        const { DashboardStore: { dashboradInfo } } = props;
        const { dashboradInfoData } = prevSate

        if (dashboradInfo !== dashboradInfoData) {
            return {
                dashboradInfoData: dashboradInfo,
            }
        }
        return null;
    }
    /**
     * This fmethod calls apis from saga based active tab just after page load.
     * Here default subtab is set and activeTab is made false.
     */
    componentDidUpdate() {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getDashboradProgramData } = this.props
        const { tabId, activeTab, dashboradTabInfoData, programDetailData } = this.state;

        if (dashboradTabInfo !== dashboradTabInfoData) {
            if (dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id) {

                if (activeTab) {
                    getDashboradProgramData({ tabId, subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id });
                }
            }
            this.setState({
                dashboradTabInfoData: dashboradTabInfo,
                subTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id,
                activeTab: dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].id ? false : true
            })
            this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[1] && dashboradTabInfo[1].updated_at })

        }

        if (programDetailData !== programDetail) {

            this.setState({
                programDetailData: programDetail
            })
        }
    }
    /**
     * This is called for version updates and fetch application for active tab.
     */
    refreshVersionInfo = async (clicked) => {
        const { DashboardStore: { dashboradTabInfo, programDetail }, getSelectedTabApplicationTypeData } = this.props
        const { tabId } = this.state;
        this.setState({ show_loader: true });
        const processDataAsycn = async () => {
            let url = `${API_URL}update-version-details`;
            let data = { env_id: (tabId === '8') ? 'versions' : tabId };

            const response = await axios({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'application/json' },
                data: data
            });
            return response;
        };
        processDataAsycn().then(async (data) => {

            if (data.code === 200) {

                await getSelectedTabApplicationTypeData({ tabId });
                this.setState({
                    dashboradTabInfoData: dashboradTabInfo,
                    programDetailData: programDetail
                })
                this.setState({ show_loader: false });
                this.setState({ 'last_updated_at': dashboradTabInfo && dashboradTabInfo[0] && dashboradTabInfo[0].updated_at });
            }
        }).catch((error) => {
        });

    }


    /**
    * Render environment for versions tab in tds for application.
    * @param {*} applications 
    */
    renderEnvironment(application) {
        return (application.environment_name) ? application.environment_name : '';
    }
  



    /**
     * This part render html with dynamic content.
     */

    render() {
        const { tabId,  last_updated_at, show_loader } = this.state
        let { DashboardStore: {  healthCheck, anchorStatus, programDetailPhase, instanceStatus },
            getSelectedTabApplicationTypeData,
            LoginStore: { user } } = this.props


        return (
            <>

                <div className="content dashboard">
                    <div id="left-panel" className="left-panel">
                        {
                            (tabId !== '9') ?
                                <button type="button" onClick={() => {
                                    if (tabId === 'TARGET GROUP') {
                                        this.getInfraHealthApi();
                                    } else if (tabId === 'anchorstatus') {
                                        this.getAnchorStatusApi();
                                    } else if (tabId === 'instancestatus') {
                                        this.getInstanceStatusApi();
                                    } else {
                                        this.refreshVersionInfo(true);
                                    }
                                }} className="btn btn-success recent-updates mb-1 button-right">
                                    <i className="fa fa-refresh"></i>
                                </button> : ''
                        }

                        {(last_updated_at && tabId !== '9' && tabId !== 'TARGET GROUP' && tabId !== 'anchorstatus' && tabId !== 'instancestatus') ?
                            <button type="button" className="btn btn-success recent-updates mb-1 button-right">
                                <span>Recent activity: {last_updated_at}

                                </span>
                            </button> : ''}


                        <TabContainer>
                            {show_loader === true || programDetailPhase === "LOADING" ? (<div className='siteloader'><div className='siteloader-inner'><img src={loader} alt='loader' /></div></div>) : ""}

                            <Tabs id="controlled-tab-example" activeKey={tabId} onSelect={(tabId) => {

                                if (tabId === 'TARGET GROUP') {
                                    this.setState({
                                        tabId
                                    })
                                    this.getInfraHealthApi();

                                } else if (tabId === 'anchorstatus') {
                                    this.setState({
                                        tabId
                                    })
                                    this.getAnchorStatusApi();
                                } else if (tabId === 'instancestatus') {
                                    this.setState({
                                        tabId
                                    })
                                    this.getInstanceStatusApi();
                                } else {
                                    this.setState({
                                        tabId
                                    }, async () => {
                                        await getSelectedTabApplicationTypeData({ tabId })
                                    })
                                }

                            }}>
                                {
                                    (user && user.details.role_id === 1) ?

                                        <Tab key='infraHealth' eventKey='TARGET GROUP' title='TARGET GROUP'>

                                            <Table className="tg-table" striped bordered hover>
                                                {healthCheck && healthCheck !== 'undefined' && Object.keys(healthCheck).map((key) =>

                                                    <tbody>
                                                        <tr className="tgaccount">
                                                            <th colSpan="3">{key.toUpperCase()}</th>
                                                        </tr>
                                                        <tr>
                                                            <th>TG NAME</th>
                                                            <th>TG STATUS</th>
                                                            <th>INSTANCE STATUS</th>
                                                        </tr>
                                                        {Object.keys(healthCheck[key]).map((targetKey) =>

                                                            <tr key={targetKey}>
                                                                <td>{targetKey}</td>
                                                                <td className={(healthCheck[key][targetKey][0] === "UNHEALTHY") ? 'unhealthy-bg' : 'healthy-bg'}>
                                                                    {(healthCheck[key][targetKey][0]).toUpperCase()}</td>
                                                                <td>{healthCheck[key][targetKey][1]}</td>

                                                            </tr>
                                                        )}
                                                    </tbody>
                                                )}
                                            </Table>
                                        </Tab> : ''}

                                {(user && user.details.role_id === 1) ?
                                    <Tab key='anchorstatus' eventKey='anchorstatus' title='ANCHOR STATUS'>
                                        <Table className="tg-instance-table" striped bordered hover>
                                            {anchorStatus && anchorStatus !== 'undefined' && Object.keys(anchorStatus).map((key) =>

                                                <tbody>
                                                    <tr className="tgaccount">
                                                        <th colSpan="4">{key.toUpperCase()}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>ANCHOR NAME</th>
                                                        <th>IP</th>
                                                        <th>INSTANCE ID</th>
                                                        <th>LAUNCH TIME</th>
                                                    </tr>
                                                    {Object.keys(anchorStatus[key]).map((targetKey) =>

                                                        <tr key={targetKey}>
                                                            <td>{targetKey}</td>
                                                            <td>{(anchorStatus[key][targetKey][0]).toUpperCase()}</td>
                                                            <td>{anchorStatus[key][targetKey][1]}</td>
                                                            <td>{anchorStatus[key][targetKey][2]}</td>
                                                        </tr>
                                                    )}
                                                </tbody>
                                            )}
                                        </Table>
                                    </Tab> : ''}

                                {(user && user.details.role_id === 1) ?
                                    <Tab key='instancestatus' eventKey='instancestatus' title='INSTANCES'>
                                        <Table className="tg-instance-table" striped bordered hover>
                                            {instanceStatus && instanceStatus !== 'undefined' && Object.keys(instanceStatus).map((key) =>

                                                <tbody>
                                                    <tr className="tgaccount">
                                                        <th colSpan="4">{key.toUpperCase()}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>INSTANCE NAME</th>
                                                        <th>IP</th>
                                                        <th>INSTANCE ID</th>
                                                        <th>LAUNCH TIME</th>
                                                    </tr>
                                                    {Object.keys(instanceStatus[key]).map((targetKey) =>

                                                        <tr key={targetKey}>
                                                            <td>{targetKey}</td>
                                                            <td>{(instanceStatus[key][targetKey][0]).toUpperCase()}</td>
                                                            <td>{instanceStatus[key][targetKey][1]}</td>
                                                            <td>{instanceStatus[key][targetKey][2]}</td>

                                                        </tr>
                                                    )}
                                                </tbody>
                                            )}
                                        </Table>
                                    </Tab> : ''}

                            </Tabs>
                        </TabContainer>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = DashboardStore => DashboardStore;
/**
 * This object calls saga functions so can use them on dashboard.
 */
const mapDispatchToProps = {
    getEnvironemntInfo: () => getEnvironemntInfo(),
    getSelectedTabApplicationTypeData: (param) => getSelectedTabApplicationTypeData(param),
    getDashboradProgramData: (param) => getDashboradProgramData(param),
    addDploymentRequestInfo: (param) => addDploymentRequestInfo(param),
    getHealthCheckInfo: () => getHealthCheckInfo(),
    getAnchorStatusInfo: () => getAnchorStatusInfo(),
    getInstanceStatusInfo: () => getInstanceStatusInfo(),
    getLogout: () => getLogout()
};
export default connect(mapStateToProps, mapDispatchToProps)(Infrastructure);
