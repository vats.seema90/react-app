import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { PropTypes } from 'prop-types';
import InputField from '../../component/inputField';
import ButtonField from '../../component/button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import loader from '../../images/loader.gif';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      first_name: '',
      last_name: ''
    };
  }
  static propTypes = {
    getUserRegisterd: PropTypes.func,
    RegisterStore: PropTypes.any,
  };

  static getDerivedStateFromProps(props) {
    return null;
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  /**
   * Login Function
   */
  registerUser = () => {
    const { getUserRegisterd } = this.props;
    const { email, password, first_name, last_name } = this.state;
    getUserRegisterd({ email, password, first_name, last_name });
    this.notify("Account registered successfully!");

  }
  notify = (message) => toast.success(message);

  render() {
    const { email, password, first_name, last_name } = this.state;
    const { RegisterStore: { phase , user } } = this.props;

    return (
      <>
      <div className="sufee-login d-flex align-content-center flex-wrap">
      <div className="container">
      <div className="login-content">
          <div className="login-logo">
              <Link to="/">
              <img className="align-content" src="https://www.hylamobile.com/wp-content/themes/hyla-mobile/images/hyla-logo-green.svg" alt="Roooter UI"/>
              </Link>
          </div>
          <div className="login-form">
          {(phase === "LOADING") ? (
          <div className='siteloader'><div className='siteloader-inner'>
            <img src={loader} alt='loader'/></div></div>
          ): ""} 
          {(user && user.code && user.code ===201) && <div><ToastContainer /></div>}

              <div className="form-group">
                  <InputField
                  field="first_name"
                  label="First Name*"
                  value={first_name || ''}
                  className="form-control" placeholder="First Name"
                  onChange={this.onChange}/>
              </div>
              <div className="form-group">
                  <InputField
                  field="last_name"
                  label="Last Name*"
                  value={last_name || ''}
                  className="form-control" placeholder="Last Name"
                  onChange={this.onChange}/>
              </div>
              <div className="form-group">
                  <InputField
                  field="email"
                  label="Email*"
                  value={email || ''}
                  className="form-control" placeholder="Email"
                  onChange={this.onChange}/>
              </div>
              <div className="form-group">
                  <InputField  
                  field="password"
                  label="Password*"
                  value={password || ''}
                  type="password"
                  onChange={this.onChange} 
                  className="form-control" placeholder="Password"/>
              </div>
              <ButtonField className="btn green-btn btn-flat m-b-30 m-t-30" variant="primary" type="submit" label="Register" onClick={this.registerUser}  />
              <div className="register-link m-t-15 custom-link m-t-15 text-center">
                  <p>Already account ? <Link to="/">Sign In Here </Link></p>
              </div>
              </div>
          </div>
      </div>
      </div>
      </>
    );
  }
}

export default Register;
