import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import axios from 'axios';

export default function (ComposedComponent) {
  class RequireAuth extends Component {
    static propTypes = {
      user: PropTypes.any,
      history: PropTypes.any,
    };

    constructor(props) {
      super(props);
      if ((props && props.user === null) || props.user.token === undefined) {
        props.history.push('/');
      }
    }

    componentDidMount() {
      const local =JSON.parse( localStorage.getItem("persist:root"))
      if ( local && local.user && JSON.parse(local.user) && JSON.parse(local.user).token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(local.user).token}`;
      }
    }

    componentDidUpdate(nextProps) {
      const { user, history } = nextProps;
      if (user && user.token === undefined) {
        
        history.push('/');

      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { user: state.LoginStore.user };
  }

  return connect(mapStateToProps)(RequireAuth);
}
