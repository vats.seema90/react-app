import axios from 'axios';
import { UNAUTH_USER } from './constants';

export default {
  setupInterceptors: (store) => {
    axios.interceptors.response.use((response) => {
      return response.data
    }, ( error) => {
      if (error.response.status === 401) {
        store.dispatch({ type: UNAUTH_USER });
      }
      return Promise.reject(error.response.data);
    });
  },
};
