import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { LoginStore } from '../containers/Login/Store/reducer';
import { DashboardStore } from '../containers/Dashboard/Store/reducer';
import { RegisterStore } from '../containers/Register/Store/reducer';
import { UsersStore } from '../containers/users/Store/reducer';
const persistConfig = {
  key: 'root',
  storage,
};

const rootReducer = combineReducers({
  LoginStore: persistReducer(persistConfig, LoginStore),
  DashboardStore,
  RegisterStore,
  UsersStore
});

export default rootReducer;
