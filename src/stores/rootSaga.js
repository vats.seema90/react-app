import { all } from 'redux-saga/effects';
import { getUserLoginSaga } from '../containers/Login/Store/saga';
import { getDashboardSaga } from '../containers/Dashboard/Store/saga';
import { getUserRegisterSaga } from '../containers/Register/Store/saga';
import { manageUsersSaga } from '../containers/users/Store/saga';
export default function* rootSaga() {
  yield all([
    getUserLoginSaga(),
    getDashboardSaga(),
    getUserRegisterSaga(),
    manageUsersSaga()
  ]);
}
