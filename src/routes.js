import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import RequireAuth from './utils/requireAuth';
import Heading from './component/Heading';
import SideMenu from './component/SiderBar';
import Footer from './component/Footer';
const Login = lazy(() => import('./containers/Login'));
const Register = lazy(() => import('./containers/Register'));
const Dashboard = lazy(() => import('./containers/Dashboard'));
const Infrastructure = lazy(() => import('./containers/Dashboard/infrastructure'));
const Deployments = lazy(() => import('./containers/Dashboard/deployments'));
const EnvironmentCollarge = lazy(() => import('./containers/Dashboard/EnvironmentCollarge'));
const UsersSettings = lazy(() => import('./containers/users/UsersSettings'));
const Routes = ({ user }) => (
  <Router>
    <div className="warper">

      <div className="main-content">
        {user && user.token !== undefined && (
          <div className="content dashboard">
            <Heading />
            <SideMenu user={user}/>
          </div>
        )
        }
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/dashboard" component={RequireAuth(Dashboard)} />
          <Route exact path="/infrastructure" component={RequireAuth(Infrastructure)} />
          <Route exact path="/deployments" component={RequireAuth(Deployments)} />
          <Route exact path="/user-settings" component={RequireAuth(UsersSettings)} />
          <Route exact path="/environment-collarge" component={RequireAuth(EnvironmentCollarge)} />
        </Suspense>

      </div>
      <Footer />

    </div>
  </Router>
);

const mapStateToProps = ({ LoginStore }) => LoginStore;
export default connect(mapStateToProps)(Routes);
