import React from 'react';
import PropTypes from 'prop-types';

const InputField = ({
  field, value, label, error, type, onChange, placeholder, disabled, onKeyPress
}) => (
  <span>
    <label className="active" htmlFor={label}>{label}</label>

    <input
      onChange={onChange}
      value={value}
      type={type}
      name={field}
      placeholder={placeholder}
      className="form-control validate"
      disabled={disabled}
      onKeyPress ={onKeyPress}
    />
    {error && <span className="help-block invalid">{error}</span>}
  </span>
);

InputField.propTypes = {
  field: PropTypes.string,
  value: PropTypes.any,
  label: PropTypes.string,
  error: PropTypes.any,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  onKeyPress: PropTypes.func
};

InputField.defaultProps = {
  type: 'text',
  disabled: false,
};

export default InputField;
