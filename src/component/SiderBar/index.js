import React from 'react';
import { PropTypes } from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
const Sidebar = (props) => (
    
        <div className="nav nav-tabs" id="nav-tab" role="tablist">
            <div className="header-left">
            <div className="navbar-header">
            <Link className="navbar-brand" to="/dashboard"><img src="https://www.hylamobile.com/wp-content/themes/hyla-mobile/images/hyla-logo-green.svg" width="90" alt="Logo" /></Link>
            <Link className="navbar-brand hidden" to="/dashboard"><img src="https://www.hylamobile.com/wp-content/themes/hyla-mobile/images/hyla-logo-green.svg" alt="Logo" /></Link>
            </div>
        </div>
        <Link 
        className={(props.location.pathname==="/" ||(props.location.pathname).indexOf('dashboard')!==-1)? "nav-item nav-link active" : "nav-item nav-link"} 
        to="/dashboard">
        <i className="menu-icon fa fa-laptop"></i>Dashboard
        </Link>
        
        {(props.user.details.role_id ===1) ?
      
        <Link 
        className={(props.location.pathname==="/infrastructure")? "nav-item nav-link active" : "nav-item nav-link"}
        to="/infrastructure">
        <i className="menu-icon fa fa-laptop"></i>Infrastructure
        </Link>
         :''}
        {(props.user.details.role_id ===1) ?
         <Link 
        className={(props.location.pathname==="/deployments")? "nav-item nav-link active" : "nav-item nav-link"} 
        to="/deployments">
        <i className="menu-icon fa fa-laptop"></i>Deployments
        </Link>
        :''}     
        </div>
  
);

Sidebar.propTypes = {
    headingName: PropTypes.string,
};

export default withRouter (Sidebar);
